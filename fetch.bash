#bash

landing_tag=
app_tag=

fetch () {
  echo "Cloning && Fetching"
  rm -rf git/
  git clone git@gitlab.com:atlasdex/landing.git git/landing
  cd git/landing

  if git tag | grep -q $landing_tag;
    then
      echo "Switching landing to tag $landing_tag"
    else
      echo "Tag not found. Break"
      exit
  fi

  git checkout $landing_tag
  cd ../..

  git clone git@gitlab.com:atlasdex/app.git git/app
  cd git/app

  if git tag | grep -q $app_tag;
    then
      echo "Switching app to tag $app_tag"
    else
      echo "Tag not found. Break"
      exit
  fi

  git checkout $app_tag
}

if [ "$#" !=  "2" ]
   then
     echo "No arguments supplied"
   else
     landing_tag=$1
     app_tag=$2
     fetch
 fi
