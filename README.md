# Docker.

## Setup.
```
sudo apt install docker docker.io containerd docker-compose git
```

### Before deploy
prepare secret files specified in stack.yml

### Deploy app
```
mkdir atlasdex && cd atlasdex
bash fetch.bash {landing_tag} {app_tag}
sudo docker-compose -f build.yml build
sudo docker stack deploy -c stack.yml atlasdex
```

### Update app
```
docker service update --force --image atlas_nginx:latest atlasdex_atlas_nginx
```

### Run in compose
```
sudo docker-compose -f build.yml build
sudo docker-compose -f stack.yml up
```
